import wave
import pyaudio
import audioop
import speech_recognition as sr




class speechRecognition():
    def __init__(self,output_file):
        self.chunk = 2048  # Record in chunks of 1024 samples
        self.sample_format = pyaudio.paInt16  # 16 bits per sample
        self.channels = 2
        self.fs = 44100  # Record at 44100 samples per second
        self.filename = output_file

        self.p = pyaudio.PyAudio()  # Create an interface to PortAudio


        self.stream = self.p.open(format=self.sample_format,
                        channels=self.channels,
                        rate=self.fs,
                        frames_per_buffer=self.chunk,
                        input=True)

        self.stream2 = self.p.open(format=self.sample_format,
                        channels=self.channels,
                        rate=self.fs,
                        frames_per_buffer=self.chunk,
                        input=True)
    def record(self):
        self.frames = []  # Initialize array to store frames
        self.volume=0
        cpt1=0
        while (self.volume<4000 or cpt1<5):
            self.data = self.stream.read(self.chunk)
            self.volume = audioop.rms(self.data, 2) 
            #print("no recording volume",self.volume)
            if self.volume>4000:
                cpt1+=1
            if self.volume<1000:
                cpt1=0
        #print("recording")
        self.frames = []
        cpt=0
        
        while (self.volume>4000 or cpt<50):
            #print(self.volume,cpt)
            self.data = self.stream2.read(self.chunk)
            self.volume = audioop.rms(self.data, 2) 
            #print("recording volume",self.volume,cpt)
            self.frames.append(self.data)
            if self.volume<2000:
                cpt+=1

        # Stop and close the stream 
        self.stream.stop_stream()
        self.stream.close()
        # Terminate the PortAudio interface
        self.p.terminate()

        #print('Finished recording')

        # Save the recorded data as a WAV file
        self.wf = wave.open(self.filename, 'wb')
        self.wf.setnchannels(self.channels)
        self.wf.setsampwidth(self.p.get_sample_size(self.sample_format))
        self.wf.setframerate(self.fs)
        self.wf.writeframes(b''.join(self.frames))
        self.wf.close()
    def recognize(self,langage):
        self.r = sr.Recognizer()
#r.recognize_google()
        self.reco = sr.AudioFile(self.filename)

        with self.reco as source:
            self.audio = self.r.record(source)
            self.language=langage
            #l='en-US'
            self.out=self.r.recognize_google(self.audio,language=self.language)

