titre;Today we will learn how to tell who you are to other people, what is your age, and where you live;Se présenter
Hello, what's your name ? ;Bonjour, comment t'appelles tu ?;hello.jpeg
My name is <name> ;Je m'appelle <name>;myname.jpeg
How old are you ?; Quel âge as tu ?;age.jpeg
I am <age> years old;J'ai <age> ans.;age2.jpeg
Where do you live ?;Ou vis tu ?;france.jpeg
I live in <ou>. ;Je vis à <ou>;toulouse.jpeg
How are you today ? ;Comment te sens tu aujourd'hui ?;forme.jpeg
I am fine, thank you ! ;Je vais bien, merci !;vaisbien.jpeg