Je vais me promener.
Je suis parti.
Maman est partie.
Je mange.
Je fais pipi.
Ils sont allés dans la forêt.
Les enfants sont sortis.
Les chiens jouent ensemble.
J'ai fini le livre.
J'ai fini de manger.
J'ai fini de lire.
J'ai fini la dictée.
J'ai gagné.
J'ai perdu.
Elle a gagné.
Elle a perdu.
Elle est finie.
Le livre est fini.
Nous nous sommes baignés dans la piscine.
Nous irons nager dans la piscine.
Je veux manger.
J'ai faim.
Il a sommeil.
Le petit garçon blond a fait une bêtise.
La musique est trop forte.
Les oiseaux volent dans le ciel.
L'oiseau vole dans le ciel.
Le chat mange un oiseau.
Demain c'est l'anniversaire de maman.
Nous allons lui offrir des cadeaux.
Il faut ranger la chambre.
J'ai acheté trop de jeux de société.
Il fait chaud.
Le ciel est bleu.
Il pleut.
Il fait froid.
Il y a du vent.
Il neige.
J'ai froid.
J'ai faim.
J'ai chaud.
J'ai sommeil.
Nos amis sont partis.
Papy et Tonton sont partis en forêt.
Maman a acheté un jeu.
La dame est tombée par terre.
La maman a aidé son enfant à faire les devoirs.
Les dames sont parties à la plage.